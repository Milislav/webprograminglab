package mk.finki.ukim.mk.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "shop_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long orderId;

    @ManyToOne
    private Balloon balloon;

    private LocalDateTime dateTime;


    public Order() {
    }

    public Order(Balloon balloon, LocalDateTime dateTime) {
        this.balloon = balloon;
        this.dateTime = dateTime;
    }
}





package mk.finki.ukim.mk.repository.inMemory.jpa;

import mk.finki.ukim.mk.model.Balloon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalloonRepository extends JpaRepository<Balloon, Long> {

}

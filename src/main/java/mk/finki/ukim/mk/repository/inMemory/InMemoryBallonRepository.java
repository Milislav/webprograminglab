package mk.finki.ukim.mk.repository.inMemory;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Manufacturer;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/*
@Repository
public class InMemoryBallonRepository {

    public Optional<Balloon> findById(Long id) {
        return DataHolder.balloonList.stream().filter(manufacturer -> manufacturer.getId().equals(id)).findFirst();
    }

    public void deleteById(Long id) {
        DataHolder.balloonList.removeIf(i -> i.getId().equals(id));
    }

    public Optional<Balloon> save(String name, String description, Manufacturer manufacturer) {
        DataHolder.balloonList.removeIf(balloon -> balloon.getName().equals(name));
        Balloon balloon = new Balloon(name, description, manufacturer);
        DataHolder.balloonList.add(balloon);
        return Optional.of(balloon);
    }

    public Optional<Balloon> edit(Long balloonId, String name, String description, Manufacturer manufacturer) {
        deleteById(balloonId);
        Balloon newBalloon = new Balloon(name, description, manufacturer);
        DataHolder.balloonList.add(newBalloon);
        return Optional.of(newBalloon);
    }


    public List<Balloon> findAllBalloons() {
        return DataHolder.balloonList;
    }

    public List<Balloon> findAllByNameOrDescription(String text) {
        return DataHolder.balloonList.stream()
                .filter(balloon -> balloon.getName().contains(text) || balloon.getDescription().contains(text))
                .collect(Collectors.toList());
    }
}


 */
package mk.finki.ukim.mk.repository.inMemory.jpa;

import mk.finki.ukim.mk.model.ShoppingCart;
import mk.finki.ukim.mk.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long> {


    Optional<ShoppingCart> findByUser(User username);
}

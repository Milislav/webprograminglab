package mk.finki.ukim.mk.repository.inMemory.jpa;

import mk.finki.ukim.mk.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}

package mk.finki.ukim.mk.repository.inMemory;
/*
import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Manufacturer;
import mk.finki.ukim.mk.model.data.DataHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class InMemoryManufacturerRepository {

    public List<Manufacturer> findAllManufacturers() {
        return DataHolder.manufacturers;
    }

    public Manufacturer findById(Long id) {
        return DataHolder.manufacturers.stream().filter(manufacturer -> manufacturer.getId().equals(id)).findFirst().get();
    }

    public void deleteById(Long id) {
        DataHolder.manufacturers.removeIf(i -> i.getId().equals(id));
    }

    public Optional<Manufacturer> save(String name, String country, String address) {
        DataHolder.manufacturers.removeIf(manufacturer -> manufacturer.getName().equals(name));
        Manufacturer manufacturer = new Manufacturer(name, country, address);
        DataHolder.manufacturers.add(manufacturer);
        return Optional.of(manufacturer);
    }

    public Optional<Manufacturer> edit(Long manufacturerId, String name, String country, String address) {
        deleteById(manufacturerId);
        Manufacturer manufacturer = new Manufacturer(name, country, address);
        DataHolder.manufacturers.add(manufacturer);
        return Optional.of(manufacturer);
    }

}

 */

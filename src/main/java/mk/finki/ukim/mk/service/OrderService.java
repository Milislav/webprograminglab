package mk.finki.ukim.mk.service;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.ShoppingCart;

import java.util.List;

public interface OrderService  {

    List<Order> listAll();

    Order makeOrder(Long balloonId, ShoppingCart shoppingCart);

}



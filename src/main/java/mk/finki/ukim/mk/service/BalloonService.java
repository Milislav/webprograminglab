package mk.finki.ukim.mk.service;

import mk.finki.ukim.mk.model.Balloon;

import java.util.List;
import java.util.Optional;

public interface BalloonService {
    List<Balloon> listAll();
    void deleteById(Long id);
    Optional<Balloon> save(String name, String description, Long manufacturer);
    Optional<Balloon> edit(Long balloonId, String name, String description, Long manufacturer);
    Optional<Balloon> findById(Long id);

}

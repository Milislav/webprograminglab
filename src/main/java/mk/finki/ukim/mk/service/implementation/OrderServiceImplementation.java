package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.ShoppingCart;
import mk.finki.ukim.mk.repository.inMemory.jpa.BalloonRepository;
import mk.finki.ukim.mk.repository.inMemory.jpa.OrderRepository;
import mk.finki.ukim.mk.service.OrderService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
public class OrderServiceImplementation implements OrderService {

    private final OrderRepository orderRepository;
    private final BalloonRepository balloonRepository;

    public OrderServiceImplementation(OrderRepository orderRepository, BalloonRepository balloonRepository) {
        this.orderRepository = orderRepository;
        this.balloonRepository = balloonRepository;
    }

    @Override
    public List<Order> listAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order makeOrder(Long balloonId, ShoppingCart shoppingCart) {
        Order newOrder = new Order(balloonRepository.getById(balloonId), LocalDateTime.now());
        return orderRepository.save(newOrder);
    }
}



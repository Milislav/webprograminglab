package mk.finki.ukim.mk.service;

import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.model.enumerations.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface UserService extends UserDetailsService {
    List<User> findAll();
    User register(String username, String password, String repeatPassword, String name, String surname, LocalDate dateOfBirth, Role role);
    void deleteByUsername(String username);
    Optional<User> findByUsername(String username);
    Optional<User> changeRole(String username, Role role);

}

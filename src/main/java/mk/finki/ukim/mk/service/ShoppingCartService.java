package mk.finki.ukim.mk.service;

import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {

    ShoppingCart addProductToShoppingCart(String username, Order order);

    ShoppingCart getActiveShoppingCart(String username);

    List<Order> listAllOrdersInShoppingCart(Long cartId);


}

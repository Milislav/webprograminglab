package mk.finki.ukim.mk.service;

import mk.finki.ukim.mk.model.Manufacturer;

import java.util.List;
import java.util.Optional;

public interface ManufacturerService {

    List<Manufacturer> listAll();
    Optional<Manufacturer> findById(Long id);
    void deleteById(Long id);
    Optional<Manufacturer> save(String name, String country, String address);
    Optional<Manufacturer> edit(Long manufacturerId, String name, String description, String address);



}

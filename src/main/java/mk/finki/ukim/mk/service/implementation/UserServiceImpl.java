package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.model.enumerations.Role;
import mk.finki.ukim.mk.model.exceptions.InvalidUsernameOrPasswordException;
import mk.finki.ukim.mk.model.exceptions.PasswordsDoNotMatchException;
import mk.finki.ukim.mk.model.exceptions.UsernameAlreadyExistsException;
import mk.finki.ukim.mk.repository.inMemory.jpa.UserRepository;
import mk.finki.ukim.mk.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User register(String username, String password, String repeatPassword, String name, String surname, LocalDate dateOfBirth, Role role) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty())
            throw new InvalidUsernameOrPasswordException();
        if (!password.equals(repeatPassword))
            throw new PasswordsDoNotMatchException();
        if (this.userRepository.findByUsername(username).isPresent())
            throw new UsernameAlreadyExistsException(username);
        User user = new User(username, passwordEncoder.encode(password), name, surname, dateOfBirth, role);
        return userRepository.save(user);
    }


    @Override
    public void deleteByUsername(String username) {
        this.userRepository.deleteByUsername(username);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> changeRole(String username, Role role) {
        User user = this.userRepository.findByUsername(username).orElseThrow();
        return Optional.of(user);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s).orElseThrow(()->new UsernameNotFoundException(s));
    }


}

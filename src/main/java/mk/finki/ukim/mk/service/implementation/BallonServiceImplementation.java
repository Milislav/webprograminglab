package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.repository.inMemory.jpa.BalloonRepository;
import mk.finki.ukim.mk.repository.inMemory.jpa.ManufacturerRepository;
import mk.finki.ukim.mk.service.BalloonService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BallonServiceImplementation implements BalloonService {

    private final BalloonRepository ballonRepository;
    private final ManufacturerRepository manufacturerRepository;

    public BallonServiceImplementation(BalloonRepository ballonRepository, ManufacturerRepository manufacturerRepository) {
        this.ballonRepository = ballonRepository;
        this.manufacturerRepository = manufacturerRepository;
    }


    @Override
    public List<Balloon> listAll() {
        return ballonRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        this.ballonRepository.deleteById(id);
    }


    @Override
    public Optional<Balloon> save(String name, String description, Long manufacturer) {
        Balloon newBalloon = new Balloon(name, description, manufacturerRepository.findById(manufacturer).get());
        this.ballonRepository.save(newBalloon);
        return Optional.of(newBalloon);
    }

    @Override
    public Optional<Balloon> edit(Long balloonId, String name, String description, Long manufacturer) {

        Balloon balloon = this.ballonRepository.findById(balloonId).orElseThrow();

        balloon.setName(name);
        balloon.setDescription(description);
        balloon.setManufacturer(manufacturerRepository.findById(manufacturer).get());

        ballonRepository.save(balloon);

        return Optional.of(balloon);
    }

    @Override
    public Optional<Balloon> findById(Long id) {
        return ballonRepository.findById(id);
    }
}



package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.Manufacturer;
import mk.finki.ukim.mk.repository.inMemory.jpa.ManufacturerRepository;
import mk.finki.ukim.mk.service.ManufacturerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ManufacturerServiceImplementation implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;

    public ManufacturerServiceImplementation(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }


    @Override
    public List<Manufacturer> listAll() {
        return manufacturerRepository.findAll();
    }

    @Override
    public Optional<Manufacturer> findById(Long id) {
        return manufacturerRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        this.manufacturerRepository.deleteById(id);
    }

    @Override
    public Optional<Manufacturer> save(String name, String country, String address) {
        Manufacturer newManufacturer = new Manufacturer(name, country, address);
        this.manufacturerRepository.save(newManufacturer);
      return Optional.of(newManufacturer);
    }

    @Override
    public Optional<Manufacturer> edit(Long manufacturerId, String name, String country, String address) {

        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId).orElseThrow();
        manufacturer.setName(name);
        manufacturer.setCountry(country);
        manufacturer.setAddress(address);

        manufacturerRepository.save(manufacturer);

        return Optional.of(manufacturer);
    }

}



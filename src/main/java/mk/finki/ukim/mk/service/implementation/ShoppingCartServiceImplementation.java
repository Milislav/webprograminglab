package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.ShoppingCart;
import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.model.exceptions.ShoppingCartNotFoundException;
import mk.finki.ukim.mk.model.exceptions.UserNotFoundException;
import mk.finki.ukim.mk.repository.inMemory.jpa.ShoppingCartRepository;
import mk.finki.ukim.mk.repository.inMemory.jpa.UserRepository;
import mk.finki.ukim.mk.service.ShoppingCartService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartServiceImplementation implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final UserRepository userRepository;

    public ShoppingCartServiceImplementation(ShoppingCartRepository shoppingCartRepository, UserRepository userRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.userRepository = userRepository;
    }


    @Override
    public ShoppingCart addProductToShoppingCart(String username, Order order) {
        ShoppingCart shoppingCart = this.getActiveShoppingCart(username);
        shoppingCart.getOrders().add(order);
        return shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public ShoppingCart getActiveShoppingCart(String username) {
        User user = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));

        return this.shoppingCartRepository
                .findByUser(user)
                .orElseGet(() -> {
                    ShoppingCart cart = new ShoppingCart(user);
                    return this.shoppingCartRepository.save(cart);
                });
    }

    @Override
    public List<Order> listAllOrdersInShoppingCart(Long cartId) {
        if(!this.shoppingCartRepository.findById(cartId).isPresent())
            throw new ShoppingCartNotFoundException(cartId);
        return this.shoppingCartRepository.findById(cartId).get().getOrders();
    }
}

package mk.finki.ukim.mk.service.implementation;

import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.model.exceptions.InvalidArgumentsException;
import mk.finki.ukim.mk.model.exceptions.InvalidUserCredentialsException;
import mk.finki.ukim.mk.repository.inMemory.jpa.UserRepository;
import mk.finki.ukim.mk.service.AuthenticationService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImplementation implements AuthenticationService {
    private final UserRepository userRepository;

    public AuthServiceImplementation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(String username, String password) {
        if (username==null || username.isEmpty() || password==null || password.isEmpty()) {
            throw new InvalidArgumentsException();
        }
        return userRepository.findByUsernameAndPassword(username,
                password).orElseThrow(InvalidUserCredentialsException::new);
    }
}

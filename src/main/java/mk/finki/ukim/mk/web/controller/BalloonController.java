package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Manufacturer;
import mk.finki.ukim.mk.service.BalloonService;
import mk.finki.ukim.mk.service.ManufacturerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/balloons")
public class BalloonController {

    private final BalloonService balloonService;
    private final ManufacturerService manufacturerService;

    public BalloonController(BalloonService balloonService, ManufacturerService manufacturerService) {
        this.balloonService = balloonService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public String getBallons(@RequestParam(required = false) String error, Model model) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        List<Balloon> balloonList = this.balloonService.listAll();
        model.addAttribute("balloons", balloonList);
        model.addAttribute("bodyContent", "listBalloonsController");
        return "master-template.html";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteBalloon(@PathVariable Long id) {
        System.out.println(id);
        this.balloonService.deleteById(id);
        return "redirect:/balloons";
    }

    @GetMapping("/edit-form/{id}")
    public String editBalloonPage(@PathVariable Long id, Model model) {
        if (this.balloonService.findById(id).isPresent()) {
            Balloon balloon = this.balloonService.findById(id).get();
            List<Manufacturer> manufacturers = this.manufacturerService.listAll();
            model.addAttribute("manufacturers", manufacturers);
            model.addAttribute("balloon", balloon);
            model.addAttribute("bodyContent", "addBalloon.html");
            return "master-template.html";
        }
        return "redirect:/balloon?error=ProductNotFound";
    }

    @GetMapping("/add-form")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addBalloonPage(Model model) {
        List<Manufacturer> manufacturers = this.manufacturerService.listAll();
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("bodyContent", "addBalloon.html");

        return "master-template.html";
    }

    @PostMapping("/add")
    public String saveBalloon(
            @RequestParam(required = false) Long id,
            @RequestParam String name,
            @RequestParam String description,
            @RequestParam Long manufacturer) {
        if (id != null) {
           this.balloonService.edit(id, name, description, manufacturer);
        } else {
            this.balloonService.save(name, description, manufacturer);
        }
        return "redirect:/balloons";
    }


}

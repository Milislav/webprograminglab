package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.ShoppingCart;
import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.service.BalloonService;
import mk.finki.ukim.mk.service.OrderService;
import mk.finki.ukim.mk.service.ShoppingCartService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/shopping-cart")
public class ShoppingCartController {

    private final ShoppingCartService shoppingCartService;
    private final BalloonService balloonService;
    private final OrderService orderService;

    public ShoppingCartController(ShoppingCartService shoppingCartService, BalloonService balloonService, OrderService orderService) {
        this.shoppingCartService = shoppingCartService;
        this.balloonService = balloonService;
        this.orderService = orderService;
    }

    @GetMapping
    public String getShoppingCartPage(@RequestParam(required = false) String error,
                                      HttpServletRequest req,
                                      Model model) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
      //  User user = (User) req.getSession().getAttribute("user"); // req.getRemoteUser();
        String username = req.getRemoteUser();
        ShoppingCart shoppingCart = this.shoppingCartService.getActiveShoppingCart(username);
        model.addAttribute("orders", shoppingCart.getOrders());
        model.addAttribute("username",username);
        model.addAttribute("bodyContent", "shoppingCart.html");

        return "master-template.html";
    }

    @PostMapping("/add-product/{id}")
    public String addProductToShoppingCart(@PathVariable Long id,
                                           @RequestParam(required = false) LocalDateTime dateTime,
                                           HttpServletRequest req,
                                           Authentication authentication) {
        try {
            // User user = (User) req.getSession().getAttribute("user");
            User user = (User) authentication.getPrincipal();
            String username = req.getRemoteUser();
            Order order = orderService.makeOrder(id, shoppingCartService.getActiveShoppingCart(username));
            this.shoppingCartService.addProductToShoppingCart(username, order);
            return "redirect:/shopping-cart";
        } catch (RuntimeException exception) {
            return "redirect:/shopping-cart?error=" + exception.getMessage();
        }
    }
}
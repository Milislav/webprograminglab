package mk.finki.ukim.mk.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @GetMapping("/access_denied")
    public String accessDenied(Model model) {
        model.addAttribute("bodyContent", "accessDenied");
        return "master-template.html";
    }
}
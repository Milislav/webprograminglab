package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Manufacturer;
import mk.finki.ukim.mk.model.User;
import mk.finki.ukim.mk.model.enumerations.Role;
import mk.finki.ukim.mk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping
    public String getUsers(Model model) {

        List<User> users = this.userService.findAll();
        model.addAttribute("users", users);
        model.addAttribute("bodyContent", "users");
        return "master-template.html";
    }

    @Transactional
    @DeleteMapping("/delete/{username}")
    public String deleteBalloon(@PathVariable String username) {
        this.userService.deleteByUsername(username);
        return "redirect:/users";
    }

    @GetMapping("/edit-form/{username}")
    public String editUser(@PathVariable String username, Model model) {
        if (this.userService.findByUsername(username).isPresent()) {
            User user = this.userService.findByUsername(username).get();
            model.addAttribute("user", user);
            model.addAttribute("roles", Role.values());
            model.addAttribute("bodyContent", "editUser.html");
            return "master-template.html";
        }
        return "redirect:/balloon?error=ProductNotFound";
    }

    @PostMapping("/changeRole")
    public String changeRole(
            @RequestParam String username,
            @RequestParam Role role) {
        System.out.println(username);
        System.out.println(role);

        return "redirect:/users";
    }
}

package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Manufacturer;
import mk.finki.ukim.mk.service.BalloonService;
import mk.finki.ukim.mk.service.ManufacturerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/manufacturers")
public class ManufacturerController {


    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public String getManufacturers(@RequestParam(required = false) String error, Model model) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        List<Manufacturer> manufacturerList = this.manufacturerService.listAll();
        model.addAttribute("manufacturers", manufacturerList);
        model.addAttribute("bodyContent", "listManufacturersController.html");
        return "master-template.html";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteBalloon(@PathVariable Long id) {
        this.manufacturerService.deleteById(id);
        return "redirect:/manufacturers";
    }

    @GetMapping("/edit-form/{id}")
    public String editManufacturerPage(@PathVariable Long id, Model model) {
        if (this.manufacturerService.findById(id).isPresent()) {
            Manufacturer manufacturer = this.manufacturerService.findById(id).get();
            model.addAttribute("manufacturer", manufacturer);
            return "addOrEditManufacturer.html";
        }
        return "redirect:/manufacturers?error=ProductNotFound"; //redirect:/balloon?error=ProductNotFound
    }

    @GetMapping("/add-form")
    public String addManufacturerPage() {
        return "addOrEditManufacturer.html";
    }

    @PostMapping("/add")
    public String saveManufacturer(
            @RequestParam(required = false) Long id,
            @RequestParam String name,
            @RequestParam String country,
            @RequestParam String address) {
        if (id != null) {
            this.manufacturerService.edit(id, name, country, address);
        } else {
            this.manufacturerService.save(name, country, address);
        }
        return "redirect:/manufacturers";
    }




}

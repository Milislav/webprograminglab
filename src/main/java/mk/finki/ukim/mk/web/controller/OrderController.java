package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    public String getOrders(@RequestParam(required = false) String error, Model model) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        List<Order> orderList = this.orderService.listAll();
        model.addAttribute("orders", orderList);
        return "listBalloonsController.html";
    }
}

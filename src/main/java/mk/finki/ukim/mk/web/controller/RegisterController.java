package mk.finki.ukim.mk.web.controller;

import mk.finki.ukim.mk.model.enumerations.Role;
import mk.finki.ukim.mk.model.exceptions.InvalidArgumentsException;
import mk.finki.ukim.mk.model.exceptions.PasswordsDoNotMatchException;
import mk.finki.ukim.mk.service.AuthenticationService;
import mk.finki.ukim.mk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final AuthenticationService authService;
    private final UserService userService;

    public RegisterController(AuthenticationService authService, UserService userService) {
        this.authService = authService;
        this.userService = userService;
    }

    @GetMapping
    public String getRegisterPage(@RequestParam(required = false) String error, Model model) {
        if(error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        return "register";
    }

    @PostMapping
    public String register(@RequestParam String username,
                           @RequestParam String password,
                           @RequestParam String repeatedPassword,
                           @RequestParam String name,
                           @RequestParam String surname,
                           @RequestParam Role role) {
        try{
            LocalDate date = LocalDate.now();
            this.userService.register(username, password, repeatedPassword, name, surname, date, role);
            return "redirect:/login";
        } catch (InvalidArgumentsException | PasswordsDoNotMatchException exception) {
            return "redirect:/register?error=" + exception.getMessage();
        }
    }
}

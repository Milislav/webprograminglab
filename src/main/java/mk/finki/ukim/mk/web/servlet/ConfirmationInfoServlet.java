package mk.finki.ukim.mk.web.servlet;
/*
import mk.finki.ukim.mk.model.Balloon;
import mk.finki.ukim.mk.model.Order;
import mk.finki.ukim.mk.model.data.DataHolder;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "thymeleaf-confirmation-balloon-servlet", urlPatterns = "/confirmationInfoBalloon")
public class ConfirmationInfoServlet extends HttpServlet {

    private final SpringTemplateEngine springTemplateEngine;
    public Long id = 0L;

    public ConfirmationInfoServlet(SpringTemplateEngine springTemplateEngine) {
        this.springTemplateEngine = springTemplateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WebContext context = new WebContext(req, resp, req.getServletContext());

        if (req.getSession().getAttribute("color") == null) {
            resp.sendRedirect("/balloonList");
        }

        String balloonColor = (String) req.getSession().getAttribute("color");

        String balloonSize = (String) req.getSession().getAttribute("size");

        String clientName = (String) req.getSession().getAttribute("clientName");

        String clientAddress = (String) req.getSession().getAttribute("clientAddress");


   //     DataHolder.orderList.add(new Order(id++, balloonColor, balloonSize, clientName, clientAddress));

        context.setVariable("balloonColorSelected", balloonColor);
        context.setVariable("balloonSizeSelected", balloonSize);
        context.setVariable("clientName", clientName);
        context.setVariable("clientAddress", clientAddress);
        context.setVariable("ipAddress", req.getRemoteAddr());
        context.setVariable("clientAgent", req.getHeader("User-Agent"));


        this.springTemplateEngine.process("confirmationInfo.html", context, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect("balloonList");
    }
}

 */
